exports.scrape = async (page) => {
  return await page.evaluate(() => {
    const data = {};

    try {
      const image = document.querySelector(
        "img.pv-top-card-profile-picture__image"
      )?.src;
      image && (data["img"] = image);
    } catch (err) {
      // ..........................
    }

    try {
      const name = document.querySelector(
        ".pv-text-details__left-panel h1.text-heading-xlarge"
      )?.innerText;
      name && (data["name"] = name);
    } catch (err) {
      // .........................
    }

    try {
      const placeOfResidance = document.querySelector(
        ".pv-text-details__left-panel span.text-body-small"
      )?.innerText;
      placeOfResidance && (data["place_of_residance"] = placeOfResidance);
    } catch (err) {
      // .......................
    }

    try {
      const informationLabel = document.querySelector(
        "div.profile-detail header.pv-profile-section__card-header"
      );
      const information = informationLabel?.nextElementSibling;
      data[informationLabel.innerText] = information?.innerText;
    } catch (err) {
      // ..........................
    }

    // EXPERIENCE
    try {
      const experienceLabel = document.querySelector(
        "#experience-section .pv-profile-section__card-header"
      );
      const experience = experienceLabel?.nextElementSibling.querySelectorAll(
        ".pv-profile-section__list-item"
      );
      if (experience && experience != undefined) {
        data[experienceLabel.innerText] = {};
        for (let experienceElement of experience) {
          const position = experienceElement.querySelector("h3").innerText;
          const labels = Array.from(
            experienceElement.querySelectorAll(".visually-hidden")
          );
          data[experienceLabel.innerText][position] = {};
          for (const label of labels) {
            data[experienceLabel.innerText][position][label.innerText] =
              label.nextElementSibling?.innerText;
          }
          const more = experienceElement.querySelector(
            "div.inline-show-more-text"
          )?.innerText;
          more && (data[experienceLabel.innerText][position].info = more);
        }
      }
    } catch (err) {
      // ........................
    }

    // EDUCATION
    try {
      const educationLabel = document.querySelector(
        "#education-section .pv-profile-section__card-header"
      );
      const education = educationLabel?.nextElementSibling.querySelectorAll(
        ".pv-profile-section__list-item"
      );
      if (education && education != undefined) {
        data[educationLabel.innerText] = {};
        for (let educationElement of education) {
          const school = educationElement.querySelector("h3").innerText;
          const labels = Array.from(
            educationElement.querySelectorAll(".visually-hidden")
          );
          data[educationLabel.innerText][school] = {};
          for (const label of labels) {
            data[educationLabel.innerText][school][label.innerText] =
              label.nextElementSibling?.innerText;
          }
          // const more = educationElement?.querySelector(
          //   "div.inline-show-more-text"
          // )?.innerText;
          // more && (data[educationElement.innerText][school].info = more);
        }
      }
    } catch (err) {
      // ............................
    }

    // SKILLS
    try {
      const skillsLabel = document.querySelector(
        ".pv-skill-categories-section .pv-profile-section__card-heading"
      );
      const skills = skillsLabel?.parentElement?.nextElementSibling.querySelectorAll(
        ".pv-skill-category-entity__name-text"
      );
      if (skills && skills != undefined) {
        data[skillsLabel.innerText] = [];
        for (let skill of skills) {
          data[skillsLabel.innerText].push(skill.innerText);
        }
      }
    } catch (err) {
      // .............................
    }

    // INTERESTs
    try {
      const interestsLabel = document.querySelector(
        ".pv-interests-section .card-heading"
      );
      const interests = interestsLabel?.nextElementSibling.querySelectorAll(
        ".pv-entity__summary-title-text"
      );
      if (interests && interests != undefined) {
        data[interestsLabel.innerText] = [];
        for (let interest of interests) {
          data[interestsLabel.innerText].push(interest.innerText);
        }
      }
    } catch (err) {
      // ........................
    }

    // ACHIEVEMENTS
    try {
      const achievementsLabel = document.querySelector(
        ".pv-accomplishments-section .card-heading"
      );
      const achievements = achievementsLabel?.parentElement?.parentElement?.querySelectorAll(
        ".pv-accomplishments-block__title"
      );
      if (achievements && achievements != undefined) {
        data[achievementsLabel.innerText] = {};
        for (let achievement of achievements) {
          data[achievementsLabel.innerText][achievement.innerText] =
            achievement?.nextElementSibling?.nextElementSibling?.innerText;
        }
      }
    } catch (err) {
      // ........................
    }

    return data;
  });
};
