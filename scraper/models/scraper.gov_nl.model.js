exports.scrape = async (page) => {
  return await page.evaluate(() => {
    const data = {};
    let labels = Array.from(document.querySelectorAll(".lo-datalist__key"));
    labels.map((label, i) => {
      let upHeader;
      let header;
      if (
        label.parentElement.parentElement.parentElement.parentElement
          .parentElement.parentElement?.previousElementSibling
      ) {
        header =
          label.parentElement.parentElement.parentElement.parentElement
            .parentElement.parentElement.previousElementSibling.innerText;
      } else {
        header =
          label.parentElement.parentElement.parentElement.parentElement
            .parentElement.parentElement.parentElement.parentElement
            .previousElementSibling.innerText;
        upHeader =
          label.parentElement.parentElement.parentElement.parentElement
            .parentElement.parentElement.parentElement.parentElement
            .previousElementSibling.parentElement.parentElement
            .previousElementSibling.innerText;
      }

      if (!upHeader) {
        if (!data[header]) data[header] = {};
      } else {
        if (!data[upHeader]) data[upHeader] = {};
        if (!data[upHeader][header]) data[upHeader][header] = {};
      }
      // !upHeader
      //   ? !data[header] && (data[header] = {})
      //   : !data[upHeader]
      //   ? (data[upHeader] = {})
      //   : !data[upHeader][header] && (data[upHeader][header] = {});

      !upHeader
        ? (data[header][label.innerText] = label.parentElement.querySelector(
            ".lo-datalist__value"
          ).innerText)
        : (data[upHeader][header][
            label.innerText
          ] = label.parentElement.querySelector(
            ".lo-datalist__value"
          ).innerText);
    });
    return data;
  });
};
