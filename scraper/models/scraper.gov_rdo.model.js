const HTMLParser = require("node-html-parser");

exports.scrape = async (page) => {
  try {
    return await page
      .evaluate(() => {
        try {
          let userData = Array.from(document.querySelectorAll(".p_data-row"));
          let identityCards = Array.from(
            document.querySelectorAll(".accordion-heading__button-text span")
          );
          let jsObject = {};
          const labels = [
            "Numer PESEL",
            "Imię (imiona)",
            "Nazwisko",
            "Nazwisko rodowe",
            "Imię ojca",
            "Imię matki",
            "Nazwisko rodowe matki",
            "Data i miejsce urodzenia",
            "Płeć",
            "Obywatelstwo",
          ];
          const identityCardLabels = [
            "Data wydania (personalizacji)",
            "Data ważności dowodu",
            "Organ, który wydał dowód",
            "Przyczyna wydania",
            "Data i godzina zawieszenia",
            "Data unieważnienia",
            "Zdjęcie",
          ];
          const IDENTITY_CARD_ALIAS = "Aktualny dowód osobisty";
          const HISTORICAL_IDENTITY_CARD_ALIAS =
            "Poprzednie numery dowodów osobistych";

          identityCards.map((identityCard, i) => {
            if (identityCard.innerText.includes("Dowód osobisty")) {
              if (jsObject[HISTORICAL_IDENTITY_CARD_ALIAS]) {
                jsObject[HISTORICAL_IDENTITY_CARD_ALIAS].push(
                  identityCards[i + 1].innerText
                );
                return;
              }
              jsObject[HISTORICAL_IDENTITY_CARD_ALIAS] = [];
              jsObject[IDENTITY_CARD_ALIAS] = {};
              jsObject[IDENTITY_CARD_ALIAS]["Numer dowodu"] = {};
              jsObject[IDENTITY_CARD_ALIAS]["Numer dowodu"] =
                identityCards[i + 1].innerText;
            }
          });

          userData.map((data) => {
            if (
              jsObject[data.children[0].innerText] ||
              jsObject[IDENTITY_CARD_ALIAS][data.children[0].innerText]
            ) {
              return;
            }

            if (identityCardLabels.indexOf(data.children[0].innerText) > -1) {
              // tworzenie obiektu z danym label
              jsObject[IDENTITY_CARD_ALIAS][data.children[0].innerText] = {};

              if (data.children[0].innerText != "Zdjęcie") {
                // wprowadzanie wartości
                jsObject[IDENTITY_CARD_ALIAS][data.children[0].innerText] =
                  data.children[1].innerText;
              } else {
                const imgSrc = data.children[1].querySelector("img").src;
                jsObject[IDENTITY_CARD_ALIAS][
                  data.children[0].innerText
                ] = imgSrc;
              }
              return;
            }

            if (labels.indexOf(data.children[0].innerText) > -1) {
              jsObject[data.children[0].innerText] = {};
              jsObject[data.children[0].innerText] = data.children[1].innerText;
            }
          });
          return jsObject;
        } catch (err) {
          return err;
        }
      })
      .catch((err) => {
        throw new Error("Error occured while scraping data: ", { err });
      });
  } catch (err) {
    throw new Error("Error occured while scraping data: ", { err });
  }
};
