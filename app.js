(async () => {
  const AppController = require("./app/controllers/app.controller");
  const fs = require("fs");
  // const { getHash } = require("./crypto/crypto");

  let bucketName;
  let bucketPath;
  let service;
  let userId;

  // data that user choose to update
  // let data;
  for (const parameter of process.argv) {
    if (parameter.includes("bucketName")) {
      const value = parameter.split("=")[1];
      bucketName = value;
    }
    if (parameter.includes("bucketPath")) {
      const value = parameter.split("=")[1];
      bucketPath = value;
      userId = value.split("/")[0];
    }
    if (parameter.includes("service")) {
      const value = parameter.split("=")[1];
      service = value;
    }
  }

  if (!bucketName) throw new Error("no bucketName parameter");
  if (!bucketPath) throw new Error("no bucketPath parameter");
  if (!service) throw new Error("no service parameter");

  try {
    // download file from storage
    const fileToScrape = await AppController.downloadFile(
      bucketName,
      bucketPath
    );

    const dataObj = await AppController.webScraping(fileToScrape, service);

    const jsonPath = await AppController.uploadData(
      bucketName,
      bucketPath.substr(0, bucketPath.lastIndexOf("/")),
      service,
      dataObj
    );

    // console.log("i have data object... ", dataObj);
    // map scraped data according to BigQuery table labels/fields with topicName where the data should be published
    let topicData = {};

    // if (service != "linkedin") {
    topicData = await AppController.dataMapper({
      data: dataObj,
      service: service,
    });

    topicData.jsonStoragePath = jsonPath;

    if (topicData.picture != undefined && topicData.picture != " ") {
      try {
        await AppController.saveImage(userId, bucketName, topicData);
      } catch (err) {
        // .....
      }
    }
    // }

    // remove file
    await fs.unlinkSync(fileToScrape);

    console.log(JSON.stringify(topicData));
  } catch (err) {
    console.log(err);
  }
})();
